﻿using System;
using System.Net.Http;
using System.Text;
using Thinktecture.IdentityModel.Client;

namespace RefreshTokenClient
{
    class Program
    {
        const string token_url = "https://blablabla.com/IDServer/connect/token";
        const string refreshTokenClient = "refresh_token_client";
        const string refreshClientSecret = "WKsiFAx2gmGt2+IydsblEO1+H3w=";
        const string username = "someUserName";
        const string password = "someUserPassword";
        const string scope = "api offline_access";

        const string api_url = "http://localhost:52031/test";
        
        private const string url = "https://blablabla.okta.com/oauth2/v1/token";
        //private const string url = "https://blablabla.oktapreview.com/oauth2/:authorizationServerId/v1/token";
        private const string clientId = "my_client_id";
        private const string secret = "my_client_secret";

        static void Main(string[] args)
        {
            using (var client = new HttpClient())
            {
                var encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(clientId + ":" + secret));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("ContentType", "application/x-www-form-urlencoded");
                var response = client.PostAsync(url, null).Result;

                var isSuccess = response.Content.ReadAsStringAsync().Result;
            }
            
            var oauthClient = new OAuth2Client(new Uri(url), clientId, secret, OAuth2Client.ClientAuthenticationStyle.None);
            var credentials = oauthClient.RequestClientCredentialsAsync().Result;
            
            var oauth = new OAuth2Client(new Uri(token_url), refreshTokenClient, refreshClientSecret);
            var result = oauth.RequestResourceOwnerPasswordAsync(username, password, scope).Result;

            if (result.IsHttpError)
            {
                Console.WriteLine("HTTP error: {0}, {1}", result.HttpErrorStatusCode, result.HttpErrorReason);
            }
            else if (result.IsError)
            {
                Console.WriteLine("OAuth2 error: {0}", result.Error);
            }
            else
            {
                while (true)
                {
                    Console.WriteLine("Access token: {0}", result.AccessToken);
                    Console.WriteLine("Refresh token: {0}", result.RefreshToken);
                    
                    var client = new HttpClient();
                    client.SetBearerToken(result.AccessToken);

                    var getresult = client.GetAsync(api_url).Result;
                    Console.WriteLine("{0}, {1}", (int)getresult.StatusCode, getresult.StatusCode);
                    if (getresult.IsSuccessStatusCode)
                    {
                        var json = getresult.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(json);
                    }

                    Console.WriteLine();
                    Console.WriteLine("Done. Hit enter to use refresh token to get new access token.");
                    Console.ReadLine();
                    Console.WriteLine();

                    result = oauth.RequestRefreshTokenAsync(result.RefreshToken).Result;
                    if (result.IsHttpError)
                    {
                        Console.WriteLine("HTTP error: {0}, {1}", result.HttpErrorStatusCode, result.HttpErrorReason);
                        break;
                    }
                    else if (result.IsError)
                    {
                        Console.WriteLine("OAuth2 error: {0}", result.Error);
                        break;
                    }
                }
            }
        }
    }
}
